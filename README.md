### Hello!

My name is _Austin Pringle_. (Yes, *Pringle*, like the brand of potato chips. There's no relation, however. 😆)

I attend Pennsylvania Western University, California. I'm pursuing a Bachelor of Science degree in Computer Information Systems, with minor in Computer Science.

## Some Other Things About Me

🛠️ I love tinkering around by self-hosting web services and writing small applications.

🐧 I live and breathe free-and-open-source software.

🎸 In my free time, I enjoy playing instruments.
